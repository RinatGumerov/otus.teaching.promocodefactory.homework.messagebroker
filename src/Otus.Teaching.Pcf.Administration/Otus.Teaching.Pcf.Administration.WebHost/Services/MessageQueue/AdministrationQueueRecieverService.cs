﻿using Microsoft.Extensions.Hosting;
using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using System.Threading.Tasks;
using System.Threading;
using System;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.WebHost.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services.MessageQueue
{
    public class AdministrationQueueRecieverService : BackgroundService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ConnectionFactory _connectionFactory;
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly IConfiguration _configuration;

        public AdministrationQueueRecieverService(IServiceProvider serviceProvider, IConfiguration configuration)
        {
            _configuration = configuration;
            
            _serviceProvider = serviceProvider;

            _connectionFactory = new ConnectionFactory() { HostName = _configuration["RabbitMQ:Host"] };

            _connection = _connectionFactory.CreateConnection();

            _channel = _connection.CreateModel();

            _channel.QueueDeclare(
                queue: "AdministrationQueue",
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            if (stoppingToken.IsCancellationRequested)
            {
                _channel.Dispose();
                _connection.Dispose();
                return Task.CompletedTask;
            }

            var consumer = new EventingBasicConsumer(_channel);

            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();

                var message = Encoding.UTF8.GetString(body);

                using var scope = _serviceProvider.CreateScope();
                if (!Guid.TryParse(message.Replace("\"", ""), out var employeeId))
                    return;
                var repository = scope.ServiceProvider.GetRequiredService<IRepository<Employee>>();
                var employee = repository.GetByIdAsync(employeeId).Result;
                if(employee == null)
                    return;
                employee.AppliedPromocodesCount++;
                repository.UpdateAsync(employee);
            };

            _channel.BasicConsume(queue: "AdministrationQueue", autoAck: true, consumer: consumer);

            return Task.CompletedTask;
        }
    }
}
