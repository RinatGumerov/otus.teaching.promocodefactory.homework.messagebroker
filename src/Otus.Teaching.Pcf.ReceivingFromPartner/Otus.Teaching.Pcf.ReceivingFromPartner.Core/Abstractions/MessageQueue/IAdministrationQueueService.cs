﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.MessageQueue
{
    public interface IAdministrationMqService
    {
        /// <summary>
        /// Sending message to queue
        /// </summary>
        /// <param name="obj">Message object</param>
        void SendMessage(object obj);

        /// <summary>
        /// Sending message to queue
        /// </summary>
        /// <param name="message">Message body</param>
        void SendMessage(string message);

    }
}
