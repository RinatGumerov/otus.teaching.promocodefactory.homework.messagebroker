﻿using System.Text;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.MessageQueue
{
    public class RabbitMqService
    {
        private readonly IConfiguration _configuration;

        public RabbitMqService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void SendMessage(object obj, string queue)
        {
            var message =  JsonConvert.SerializeObject(obj);
            SendMessage(message, queue);
        }

        public void SendMessage(string message, string queue)
        {
            var factory = new ConnectionFactory() { HostName = _configuration["RabbitMq:Host"] };
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();

            channel.QueueDeclare(queue: queue,
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            var body = Encoding.UTF8.GetBytes(message);

            channel.BasicPublish(exchange: "",
                routingKey: queue,
                basicProperties: null,
                body: body);
        }
    }
}
