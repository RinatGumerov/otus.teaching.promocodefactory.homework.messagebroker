﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.MessageQueue;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.MessageQueue
{
    public class GivingToCustomerQueueService : IGivingToCustomerMqService
    {
        private readonly RabbitMqService _rabbitMqQueueService;
        public GivingToCustomerQueueService(IConfiguration configuration)
        {
            _rabbitMqQueueService = new RabbitMqService(configuration);
        }

        /// <inheritdoc />
        public void SendMessage(object obj)
        {
            var promoCode = (PromoCode)obj;
            var dto = new GivePromoCodeToCustomerDto()
            {
                PartnerId = promoCode.Partner.Id,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PreferenceId = promoCode.PreferenceId,
                PromoCode = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                PartnerManagerId = promoCode.PartnerManagerId
            };

            _rabbitMqQueueService.SendMessage(dto, "GivingToCustomerQueue");
        }

        /// <inheritdoc />
        public void SendMessage(string message)
        {
            _rabbitMqQueueService.SendMessage(message, "GivingToCustomerQueue");
        }
    }
}
