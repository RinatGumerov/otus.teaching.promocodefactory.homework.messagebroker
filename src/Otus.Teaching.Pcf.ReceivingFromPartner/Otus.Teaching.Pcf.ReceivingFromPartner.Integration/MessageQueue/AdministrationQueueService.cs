﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.MessageQueue;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.MessageQueue
{
    public class AdministrationService : IAdministrationMqService
    {
        private readonly RabbitMqService _rabbitMqQueueService;

        public AdministrationService(IConfiguration configuration)
        {
            _rabbitMqQueueService = new RabbitMqService(configuration);
        }

        /// <inheritdoc />
        public void SendMessage(object obj)
        {
            _rabbitMqQueueService.SendMessage(obj, "AdministrationQueue");
        }

        /// <inheritdoc />
        public void SendMessage(string message)
        {
            _rabbitMqQueueService.SendMessage(message, "AdministrationQueue");
        }
    }
}
