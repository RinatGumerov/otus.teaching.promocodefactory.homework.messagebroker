﻿using Microsoft.Extensions.Hosting;
using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using System.Threading.Tasks;
using System.Threading;
using System;
using System.Linq;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services.MessageQueue
{
    public class GivingToCustomerQueueRecieverService : BackgroundService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ConnectionFactory _connectionFactory;
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly IConfiguration _configuration;

        public GivingToCustomerQueueRecieverService(IServiceProvider serviceProvider, IConfiguration configuration)
        {
            _configuration = configuration;
            
            _serviceProvider = serviceProvider;

            _connectionFactory = new ConnectionFactory() { HostName = _configuration["RabbitMQ:Host"] };

            _connection = _connectionFactory.CreateConnection();

            _channel = _connection.CreateModel();

            _channel.QueueDeclare(
                queue: "GivingToCustomerQueue",
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            if (stoppingToken.IsCancellationRequested)
            {
                _channel.Dispose();
                _connection.Dispose();
                return Task.CompletedTask;
            }

            var consumer = new EventingBasicConsumer(_channel);

            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();

                var message = Encoding.UTF8.GetString(body);

                var givePromoCodeRequest = JsonConvert.DeserializeObject<GivePromoCodeRequest>(message);

                using var scope = _serviceProvider.CreateScope();
                var preferencesRepository = scope.ServiceProvider.GetRequiredService<IRepository<Preference>>();
                var customersRepository = scope.ServiceProvider.GetRequiredService<IRepository<Customer>>();
                var promoCodesRepository = scope.ServiceProvider.GetRequiredService<IRepository<PromoCode>>();

                var preference = preferencesRepository.GetByIdAsync(givePromoCodeRequest.PreferenceId).Result;

                if (preference == null)
                {
                    return;
                }

                //  Получаем клиентов с этим предпочтением:
                var customers = customersRepository
                    .GetWhere(d => d.Preferences.Any(x =>
                        x.Preference.Id == preference.Id)).Result;

                PromoCode promoCode = PromoCodeMapper.MapFromModel(givePromoCodeRequest, preference, customers);

                promoCodesRepository.AddAsync(promoCode);
            };

            _channel.BasicConsume(queue: "GivingToCustomerQueue", autoAck: true, consumer: consumer);

            return Task.CompletedTask;
        }
    }
}
